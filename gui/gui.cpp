

#include <iostream>
#include <QWidget>
#include <QApplication>
#include <QPushButton>
#include <QMessageBox>
#include <QFileDialog>
// #include <QLabel>

#include "sort_algo.hpp"

using namespace std;
vector<string> filename_vec;
vector<Mat> results;
vector<Mat> failures;
QImage mat2qim(Mat & mat)
{
	cvtColor(mat, mat, COLOR_BGR2RGB);
	QImage qim((const unsigned char*)mat.data, mat.cols, mat.rows, mat.step, 
	        	QImage::Format_RGB888);
	return qim;
}

QWidget *img_widget;  
class ChooseFileBtn: public QPushButton
{
    // QWidget *msg_img_widget;  

private:
public:
    ChooseFileBtn(const QString &text, QWidget *parent = Q_NULLPTR):QPushButton(text,parent)
    {
        // img_widget = new QWidget(parent);
    }
    // ~ChooseFileBtn();
    
    void mouseReleaseEvent(QMouseEvent *e)
    {
        
        QStringList fileName = QFileDialog::getOpenFileNames(
            this, 
            QString("select files."),
            "../img", 
            QString("images(*.png *jpg *bmp);;video files(*.avi *.mp4 *.wmv);;All files(*.*)"));
        if (fileName.isEmpty()) {
            QMessageBox::warning(this, "Warning!", "No file(s) selected!");
        }
        else {
            filename_vec.clear();
            results.clear();
            failures.clear();
            for (int i = 0; i < fileName.size(); i++)
            {
                filename_vec.push_back(fileName[i].toStdString());
            }
            sort_algo(filename_vec, results, failures);
            string info_str = "Success "+to_string(results.size())+" images\nFail " + to_string(failures.size())+" images";
            QString qstr = QString::fromStdString(info_str);
            // cout << results.size() << " " << failures.size() << endl;

            QMessageBox::information(this, "Finished", qstr);

            
            // cout <<  << endl;
        }
        // cout << "released!" << endl;
    }
};
class ShowSuccessBtn: public QPushButton
{
private:
    QWidget *parent;
    QPalette palette;
public:
    ShowSuccessBtn(const QString &text, QWidget *parent = Q_NULLPTR):QPushButton(text,parent)
    {
        this->parent = parent;
    }

    void mouseReleaseEvent(QMouseEvent *e)
    {
        if (results.empty())
        {
            QMessageBox::warning(this, "Warning!", "No success image(s)");
            return;
        }
        // img_widget = new QWidget(parent);
        
        QImage img =  mat2qim(results[0]);

        // img_widget->resize(800, 600); 
        img_widget->setAutoFillBackground(true);
        palette.setBrush(img_widget->backgroundRole(), QBrush(img));
        cout << results[0].cols << " " << results[0].rows << " "  
                <<  1.0f*results[0].rows/results[0].cols*800.f << endl;
        img_widget->resize(800, 1.0f*results[0].rows/results[0].cols*800.f); 
        palette.setBrush(img_widget->backgroundRole(), QBrush(img.scaled(img_widget->size())));
        img_widget->setPalette(palette);
        img_widget->show();
        results.erase(results.begin());

    }
};


class ShowFailBtn: public QPushButton
{
private:
    QWidget *parent;
    QPalette palette;
public:
    ShowFailBtn(const QString &text, QWidget *parent = Q_NULLPTR):QPushButton(text,parent)
    {
        this->parent = parent;
        // img_widget = new QWidget(parent);
    }

    void mouseReleaseEvent(QMouseEvent *e)
    {
        if (failures.empty())
        {
            QMessageBox::warning(this, "Warning!", "No fail image(s)");
            return;
        }
        
        QImage img =  mat2qim(failures[0]);

        // img_widget->resize(800, 600); 
        img_widget->setAutoFillBackground(true);
        palette.setBrush(img_widget->backgroundRole(), QBrush(img));
        // cout << results[0].cols << " " << results[0].rows << " "  
        //         <<  1.0f*results[0].rows/results[0].cols*800.f << endl;
        img_widget->resize(800, 1.0f*failures[0].rows/failures[0].cols*800.f); 
        palette.setBrush(img_widget->backgroundRole(), QBrush(img.scaled(img_widget->size())));
        img_widget->setPalette(palette);
        img_widget->show();
        failures.erase(failures.begin());

    }
};

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget *window = new QWidget();  
    window->resize(800, 800); 
    window->show(); 

    // QWidget *img_widget = new QWidget(window);  

    // img_widget->resize(800, 600); 
    // img_widget->setAutoFillBackground(true);
    // QImage image;
    // QPalette palette;
    // image.load("/mnt/d/image_stitch/img/1.jpg"); // 指定图片所在位置及图片名
    // palette.setBrush(img_widget->backgroundRole(), QBrush(image.scaled(img_widget->size())));

    // // palette.setBrush(window->backgroundRole(),QBrush(image));
    // img_widget->setPalette(palette);
    // // img->setStyleSheet("background-color:blue");

    // img_widget->show();
 
    ChooseFileBtn *button = new ChooseFileBtn(QString("Choose File"), window);  
    button->resize(200, 50); 
    button->move(100, 700);  
    button->show();
    ShowSuccessBtn *show_success_btn = new ShowSuccessBtn(QString("Show Next Success"), window);  
    show_success_btn->resize(200, 50); 
    show_success_btn->move(300, 700);  
    show_success_btn->show();
    ShowFailBtn *show_fail_btn = new ShowFailBtn(QString("Show Next Fail"), window);  
    show_fail_btn->resize(200, 50); 
    show_fail_btn->move(500, 700);  
    show_fail_btn->show();
    img_widget = new QWidget(window);

    // button->keyReleaseEvent()

    return a.exec();
}
