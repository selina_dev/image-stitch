#if !defined(__IMAGE_STITCH_HPP__)
#define __IMAGE_STITCH_HPP__

#include "opencv2/core.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/ml.hpp"

using namespace cv;
using namespace std;
using namespace cv::xfeatures2d;
using namespace cv::ml;

typedef enum
{
    ORDER_RIGHT,
    ORDER_WRONG
} IMG_ORDER;

void set_img_output(bool enable);
void OptimizeSeam(Mat &img1, Mat &trans, Mat &dst);
bool try_match(Mat &a, Mat &b, Mat &result, float &quality, IMG_ORDER &order);
float calc_homo(Mat &a, Mat &b, Mat &homo);
void CalcCorners(const Mat &H, const Mat &src);

#endif // __IMAGE_STITCH_HPP__
