#include "image_stitch.hpp"
#include "sort_algo.hpp"

int main()
{
    float quality;
    IMG_ORDER order;
    Mat result;
    Mat a, b;
    a = imread("/mnt/d/image_stitch/img/IMG_20220316_163021.jpg", 1);
    resize(a, a, Size(1024, 720), 0, 0, INTER_AREA);
    b = imread("/mnt/d/image_stitch/img/IMG_20220316_163023.jpg", 1);
    resize(b, b, Size(1024, 720), 0, 0, INTER_AREA);
    set_img_output(1);
    try_match(a, b, result, quality, order);
    waitKey();
    return 0;
}
